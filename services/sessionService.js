const db = require('../models')

const sessionService = {
  async createSession (user) {
    try {
      await db.sessions.destroy({
        where: {
          user_id: user.user_id
        }
      })

      let now = new Date()
      now.setDate(now.getDate() + 1) // add 24 hours
      let data = `{role_id:${user.role_id}}`
      let session = await db.sessions.create({
        user_id: user.user_id,
        session_id: await db.sessions.generateUUID(),
        expires: now,
        data: data
      })

      await session.save()
      console.log('Save Session')

      return [true, session.session_id]
    } catch (error) {
      throw error
    }
  }
}
module.exports = sessionService
