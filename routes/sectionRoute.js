const sectionRouter = require('express').Router()
const sectionController = require('../controllers/sectionController')
const middlewares = require('../middlewares')

sectionRouter.use(middlewares.isAuthenticated)

// end point to create new section
sectionRouter.post('/create', sectionController.createSection)
// Update Section based on section ID
sectionRouter.post('/update', sectionController.updateSection)
// Find Section based on section ID
sectionRouter.get('/find/:id', sectionController.findSectionById)
// assigned
sectionRouter.post('/assigned', sectionController.getAssigned)
// delete
sectionRouter.post('/delete', sectionController.deleteSection)
// end point find sections based on section category id
sectionRouter.post('/sections', sectionController.findSectionsByCategory)
// end point find sections based on section package id or all, ??? unused
sectionRouter.post('/findsections', sectionController.findSectionsByCategoryOrAll)
// end point to all groups  returned as array with id and group name
sectionRouter.post('/groups', sectionController.getGroups)
// end point to all areas returned as array with id and area name
sectionRouter.post('/areas', sectionController.getAreas)
// find all sections by created by user_id
sectionRouter.post('/mysections', sectionController.getMySections)
// delete section and its dependencies
sectionRouter.post('/deletesection', sectionController.deleteSection2)
// end point to all areas returned as array with id and area name
sectionRouter.post('/sectionsmenu', sectionController.sectionMenu)

module.exports = sectionRouter
