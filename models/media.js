module.exports = function (sequelize, DataTypes) {
  const Media = sequelize.define('media', {
    media_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    filename: {
      type: DataTypes.STRING,
      allowNull: false
    },
    package_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    media_type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fullpath: {
      type: DataTypes.STRING,
      allowNull: true
    },
    thumb: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    modified_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  })

  Media.associate = (models) => {
    Media.belongsTo(models.packages, {
      foreignKey: 'package_id',
      sourceKey: 'package_id',
      constraints: false
    })
  }

  return Media
}
