const db = require('../models')

const pageService = {
  async getRecent (user, paginationlimit) {
    try {
      const pages = await db.pages.findAll({
        include: [{
          model: db.modules,
          attributes: ['module_id', 'module_name'],
          include: [{
            model: db.packages,
            attributes: ['package_name', 'id']
          }],
          distinct: true
        }],
        where: {
          modified_by: user.user_id
        },
        order: [[db.Sequelize.col('updatedAt'), 'DESC']],
        limit: paginationlimit
      })
      return pages
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}
module.exports = pageService
