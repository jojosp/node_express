const db = require('../models')

module.exports = async () => {
  try {
    await db.sequelize.authenticate()
    console.log(`Connected to database : ${db.sequelize.config.database} as ${db.sequelize.config.username} at ${db.sequelize.config.host}:${db.sequelize.config.port}`)
    console.log('Sequelize config', db.sequelize.config)

    await db.sequelize.sync({force: false})
  } catch (err) {
    console.error(err)
  }
}
