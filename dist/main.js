"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import 'dotenv/config'
// require('dotenv').config()
var express = require('express');

var config = require('../config');

var env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';

var initApp = require('../loaders');

var _require = require('../loaders/logger'),
    logger = _require.logger;

function startServer() {
  return _startServer.apply(this, arguments);
}

function _startServer() {
  _startServer = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var server;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            server = express(); // server.use(express.static('../projects_dist'))

            _context.prev = 1;
            _context.next = 4;
            return initApp(server);

          case 4:
            _context.next = 9;
            break;

          case 6:
            _context.prev = 6;
            _context.t0 = _context["catch"](1);
            console.log(_context.t0);

          case 9:
            server.listen(config[env].port, function (err) {
              if (err) {
                console.log(err);
                return;
              }

              console.log("The server is running at http://localhost:".concat(config[env].port, "/"));
              console.log("The environment is,\n    ".concat(env, " : "), config[env]);
            });

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 6]]);
  }));
  return _startServer.apply(this, arguments);
}

startServer();
module.exports = {
  mylogger: logger.mylogger
};