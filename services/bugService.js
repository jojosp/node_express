const db = require('../models')

const bugService = {
  async getBugs () {
    try {
      let bugs = await db.bug.findAll({
        order: [['completed', 'ASC'], ['created_at', 'DESC']]
      })
      return bugs
    } catch (error) {
      throw error
    }
  },
  async countAllBugs () {
    try {
      let allBugs = await db.bug.findAll({
        attributes: [[db.modules.sequelize.fn('COUNT', 'bug_id'), 'count']]
      })
      return allBugs[0].dataValues.count
    } catch (error) {
      throw error
    }
  },
  async findBugsWithPagination (limit, offset) {
    try {
      let bugs = await db.bug.findAll({
        order: [['completed', 'ASC'], ['created_at', 'DESC']],
        limit: limit,
        offset: offset
      })
      return bugs
    } catch (error) {
      throw error
    }
  },
  getBug (bugId) {
    return db.bug.findById(bugId).then(bug => {
      return bug
    }).catch((error) => {
      throw error
    })
  },
  addBug (newBug) {
    return db.bug.findOrCreate({
      where: {
        package_id: newBug.package_id,
        module_id: newBug.module_id,
        page_id: newBug.page_id,
        bug_issuer: newBug.bug_issuer,
        component_name: newBug.component_name,
        bug_title: newBug.bug_title,
        bug_description: newBug.bug_description
      }
    }).catch((error) => {
      throw error
    })
  },
  updateBug (bugId, fields) {
    return db.bug.findById(bugId).then(bug => {
      if (bug) {
        bug.update(fields)
          .then(res => { return res })
          .catch(error => { throw error })
      }
      return bug
    }).catch((error) => {
      throw error
    })
  },
  changeBugStatus (bugId) {
    return db.bug.findById(bugId).then(bug => {
      if (bug) {
        bug.dataValues.completed = !bug.dataValues.completed
        bug.save()
      }
      return bug
    }).catch((error) => {
      throw error
    })
  },
  deleteBug (bugId) {
    return db.bug.findById(bugId).then(bug => {
      if (bug) {
        bug.destroy()
        return true
      }
      return bug
    }).catch((error) => {
      throw error
    })
  },
  // search based on property input using pagination
  searchFieldsPagination (property, offset, limit) {
    let condition = this.createQueryConditions(property)
    return db.bug.findAll({
      where: {$and: condition},
      order: [['completed', 'ASC'], ['created_at', 'DESC']],
      limit: limit,
      offset: offset
    }).then(bug => {
      return bug
    }).catch((error) => {
      throw error
    })
  },
  // search based on property input and only return the count
  searchFieldsCount (property) {
    let condition = this.createQueryConditions(property)
    return db.bug.findAll({
      where: {$and: condition},
      order: [['completed', 'ASC'], ['created_at', 'DESC']]
    }).then(bug => {
      return bug.length
    }).catch((error) => {
      throw error
    })
  },
  // create an array of the objects that we want to search for
  // objects with property name 'completed' and 'reopen', have a boolean value
  // rest of the objects have a string value, therefore operator like is used
  createQueryConditions (properties) {
    let query = []
    for (let i in properties) {
      if ((i === 'completed') || (i === 'reopen')) {
        query.push({[i]: properties[i]})
      } else {
        query.push({[i]: {[db.sequelize.Op.like]: `%${properties[i]}%`}})
      }
    }
    return query
  }

}
module.exports = bugService
