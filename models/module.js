// var sequelize = require('../server/main.js')
module.exports = function (sequelize, DataTypes) {
  const Module = sequelize.define('modules', {
    module_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    package_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    module_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    module_status: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    module_order: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    modified_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  },
  {
    hooks: {
      beforeCreate: (modules, options) => {
        return sequelize.query('SELECT exportflag FROM packages WHERE id = ?', {
          replacements: [modules.package_id],
          type: sequelize.QueryTypes.SELECT
        }).then((data) => {
          if (data[0].exportflag === 1) {
            throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
          }
        }).catch((error) => {
          throw new Error(error)
        })
      },
      beforeDelete: (modules, options) => {
        return sequelize.query('SELECT exportflag FROM packages WHERE id = ?', {
          replacements: [modules.package_id],
          type: sequelize.QueryTypes.SELECT
        }).then((data) => {
          if (data[0].exportflag === 1) {
            throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
          }
        }).catch((error) => {
          throw new Error(error)
        })
      },
      beforeUpdate: (modules, options) => {
        return sequelize.Promise.try(() => {
          return sequelize.query('SELECT exportflag FROM packages WHERE id = ?', {
            replacements: [modules.package_id],
            type: sequelize.QueryTypes.SELECT
          }).then((data) => {
            if (data[0].exportflag === 1) {
              throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
            }
          }).catch((error) => {
            throw new Error(error)
          })
        })
      }
    }
  })
  Module.associate = (models) => {
    Module.belongsTo(models.packages, {
      foreignKey: 'package_id',
      sourceKey: 'id'
    })
    // Module.belongsTo(models.pages, {
    //   as: 'module',
    //   foreignKey: 'module_id',
    //   sourceKey: 'module_id'
    // })
    Module.belongsTo(models.users, {
      as: 'module_modifiedby',
      foreignKey: 'modified_by',
      sourceKey: 'user_id'
    })
    Module.belongsTo(models.users, {
      as: 'module_createdby',
      foreignKey: 'created_by',
      sourceKey: 'user_id'
    })
    Module.hasMany(models.pages, {
      foreignKey: 'module_id',
      sourceKey: 'module_id'
    })
    Module.hasMany(models.bug, {
      as: 'Bugs',
      foreignKey: 'module_id',
      constraints: false
    })
  }

  return Module
}
