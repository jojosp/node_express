module.exports = function (sequelize, DataTypes) {
  const Page = sequelize.define('pages', {
    page_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    page_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    page_order: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    page_json: {
      type: DataTypes.TEXT,
      defaultValue: '[]',
      get: function () {
        return this.getDataValue('page_json')
      },
      set: function (value) {
        this.setDataValue('page_json', JSON.stringify(value))
      }
    },
    locked: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    locked_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    digi_activity_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    digi_activity_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    digi_module_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    digi_module_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    digi_skilltype_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    digi_skilltype_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    digi_keywords: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    digi_category: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    digi_weight: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    digi_eleclimitation: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    modified_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    module_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  },
  {
    hooks: {
      beforeCreate: (page, options) => {
        return sequelize.query('SELECT exportflag FROM packages INNER JOIN modules ON packages.id = modules.package_id WHERE modules.module_id = ?', {
          replacements: [page.module_id],
          type: sequelize.QueryTypes.SELECT
        }).then((data) => {
          if (data[0].exportflag === 1) {
            throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
          }
        })
      },
      beforeDestroy: (page, options) => {
        return sequelize.query('SELECT exportflag FROM packages INNER JOIN modules ON packages.id = modules.package_id INNER JOIN pages ON pages.module_id = modules.module_id WHERE pages.page_id = ?', {
          replacements: [page.page_id],
          type: sequelize.QueryTypes.SELECT
        }).then((data) => {
          if (data[0].exportflag === 1) {
            throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
          }
        }).catch((error) => {
          throw new Error(error)
        })
      },
      beforeUpdate: (page, options) => {
        return sequelize.query('SELECT exportflag FROM packages INNER JOIN modules ON packages.id = modules.package_id INNER JOIN pages ON pages.module_id = modules.module_id WHERE pages.page_id = ?', {
          replacements: [page.page_id],
          type: sequelize.QueryTypes.SELECT
        }).then((data) => {
          if (data[0].exportflag === 1) {
            throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
          } else {
            sequelize.query('INSERT INTO pageshistory (page_id, created, created_by, modified, modified_by, page_json) VALUES (:page_id, :created, :created_by, :modified, :modified_by, :page_json);', {
              replacements: {page_id: page.page_id, created: new Date().toISOString().slice(0, 19).replace('Z', ' '), created_by: page.modified_by, modified: new Date().toISOString().slice(0, 19).replace('Z', ' '), modified_by: page.modified_by, page_json: page.page_json},
              type: sequelize.QueryTypes.INSERT
            }).then((data) => {
            }).catch((error) => {
              // console.log(new Error('History Error'))
              // console.log(error)
            })
          }
        }).catch((error) => {
          throw new Error(error)
        })
      }
    }
  })

  Page.associate = (models) => {
    Page.hasMany(models.modules, {
      foreignKey: 'module_id',
      sourceKey: 'module_id',
      constraints: false
    })
    Page.belongsTo(models.users, {
      as: 'page_modifiedby',
      foreignKey: 'modified_by',
      sourceKey: 'user_id'
    })
    Page.belongsTo(models.users, {
      as: 'page_createdby',
      foreignKey: 'created_by',
      sourceKey: 'user_id'
    })
    Page.belongsTo(models.users, {
      as: 'pagelockedbyuser',
      foreignKey: 'locked_by',
      sourceKey: 'user_id'
    })
    Page.hasMany(models.bug, {
      as: 'Bugs',
      foreignKey: 'page_id',
      constraints: false
    })
  }

  return Page
}
