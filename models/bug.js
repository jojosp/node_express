module.exports = function (sequelize, DataTypes) {
  const Bug = sequelize.define(
    'bug',
    {
      bug_id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      package_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      module_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      page_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      bug_issuer: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      component_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bug_title: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bug_description: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      modified_by: {
        type: DataTypes.INTEGER,
        defaultValue: 1
      },
      completed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      reopen: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      }
    },
    {underscored: true,
      paranoid: true,
      hooks: {}
    }
  )

  Bug.associate = (models) => {
    Bug.belongsTo(models.packages, {
      as: 'package',
      foreignKey: 'package_id',
      constraints: false})
    Bug.belongsTo(models.modules, {
      as: 'module',
      foreignKey: 'module_id',
      constraints: false
    })
    Bug.belongsTo(models.pages, {
      as: 'page',
      foreignKey: 'page_id',
      targetKey: 'page_id',
      constraints: false
    })
    Bug.belongsTo(models.users, {
      as: 'bugIssuer',
      foreignKey: 'bug_issuer',
      constraints: false
    })
  }

  Bug.prototype.getIssuerUsername = function () {
    return this.getBugIssuer().then(user => {
      return Promise.resolve(user.username)
    }).catch(err => { throw new Error(err) })
  }
  Bug.prototype.getPackageName = function () {
    return this.getPackage().then(pckg => {
      return Promise.resolve(pckg.package_name)
    }).catch(err => { throw new Error(err) })
  }
  Bug.prototype.getModuleName = function () {
    return this.getModule().then(module => {
      return Promise.resolve(module.module_name)
    }).catch(err => { throw new Error(err) })
  }
  Bug.prototype.getPageName = function () {
    return this.getPage().then(page => {
      return Promise.resolve(page.page_name)
    }).catch(err => { throw new Error(err) })
  }

  return Bug
}
