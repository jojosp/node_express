const serviceContainer = require('../services')

const bugListenerFunctions = {
  notifyAdminBugEmail (bug) {
    serviceContainer.mailService.generateTestTransport().then((transporter) => {
      return serviceContainer.mailService.sendMail(transporter)
    })
      .then((info) => {
        return serviceContainer.mailService.getPreviewUrl(info)
      })
      .catch(error => {
        throw new Error(error)
      })
  },
  async notifyAdminBugSlack (bug) {
    try {
      let slackMSG = await serviceContainer.slackService.generateBugMessage(bug)

      let slackResponse = await serviceContainer.slackService.sendSlackMessage(slackMSG)
      // log response to  implement
      return null
    } catch (err) {
      throw new Error(err)
    }
  }
}
module.exports = bugListenerFunctions
