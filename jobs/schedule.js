const db = require('../models')
var schedule = require('node-schedule')
var pe = require('../helpers/errorConfig')
var path = require('path')
var fs = require('fs')
var mysqldump = require('mysqldump')
const config = require('../config')
const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development'

var jobSessionclear = schedule.scheduleJob('0 22 * * *', function () {
  db.sessions.findAll({
    where: {
      expires: {
        $lt: new Date()
      }
    }
  }).then((sessionsArray) => {
    console.log(sessionsArray.length)
    if (sessionsArray) {
      sessionsArray.forEach(function (session) {
        session.destroy()
      })
    }
  }).catch((err) => {
    console.log(pe.render(err))
  })
})

var jobCleartmp = schedule.scheduleJob('0 0 * * *', function () {
  fs.readdir('../tmp/', (err, allfiles) => {
    if (err) throw err

    for (const onefile of allfiles) {
      fs.unlink(path.join('../tmp/', onefile), err => {
        if (err) throw err
      })
    }
  })
})

var jobSqldumpbackup = schedule.scheduleJob('0 0 * * *', function () {
  var logDirectory = path.join(__dirname, '../', 'sqldump_backup/')
  var date = new Date()
  var fileDate = 'database_' + date.getDate().toString() + '_' + (date.getMonth() + 1).toString() + '_' + date.getFullYear().toString() + '.sql'
  mysqldump({
    host: config[env].host,
    user: config[env].dbuser,
    password: config[env].dbpwd,
    database: config[env].dbname,
    ifNotExist: true,
    disableForeignKeyChecks: true,
    dest: (logDirectory + fileDate) // destination file
  }, (err) => {
    console.log(err)
  })
})

var jobPagehistoryclear = schedule.scheduleJob('0 0 * * *', function () {
  let now = new Date()
  now.setDate(now.getDate() - 180) // clear page history after a 6 month
  now.toISOString().slice(0, 19).replace('Z', ' ')
  db.pageshistory.findAll({
    where: {
      updatedAt: {
        $lt: now
      }
    }
  }).then((pageshistoryArray) => {
    console.log(pageshistoryArray.length)
    if (pageshistoryArray) {
      pageshistoryArray.forEach(function (page) {
        page.destroy()
      })
    }
  }).catch((err) => {
    console.log(pe.render(err))
  })
})
// to be removed
exports.updatesession = function (session) {
  let currentsession = new Date(session.expires)
  currentsession.setSeconds(currentsession.getSeconds() + 6000)
  session.update({
    expires: currentsession
  })
}
