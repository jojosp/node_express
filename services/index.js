// import authenticationService from './authenticationService'
const authenticationService = require('./authenticationService')
const bugService = require('./bugService')
const packageService = require('./packageService')
const pageService = require('./pageService')
const userService = require('./userService')
const sessionService = require('./sessionService')
const mailService = require('./mailService')
const slackService = require('./slackService')

const servicesContainer = {bugService, userService, sessionService, packageService, pageService, mailService, slackService, authenticationService}
module.exports = servicesContainer
