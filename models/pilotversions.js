module.exports = function (sequelize, DataTypes) {
  return sequelize.define('pilotversions', {
    pilot_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    pilot_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isUniquePackagename (value, next) {
          sequelize.query('SELECT count(1) as counter FROM pilot_versions WHERE pilot_name = ? ', {
            replacements: [value],
            type: sequelize.QueryTypes.SELECT
          }).then(pilots => {
            if (pilots[0].counter > 0) {
              next('Pilot name is already in use')
            } else {
              next()
            }
          })
        },
        is: {
          args: /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/i,
          msg: 'This is not valid Pilot Name'
        }
      }
    },
    pilot_foldername: {
      type: DataTypes.STRING,
      allowNull: false
    },
    pilot_version: {
      type: DataTypes.DECIMAL(2, 1),
      allowNull: false,
      validate: {
        isMaxVersion (value, next) {
          console.log(value)
          sequelize.query('SELECT MAX(pilot_version) as maxpilot FROM pilot_versions', {
            type: sequelize.QueryTypes.SELECT
          }).then((pilots) => {
            console.log(pilots[0].maxpilot, value)
            if (pilots[0].maxpilot === null) {
              next()
            } else {
              if (pilots[0].maxpilot >= value) {
                next('Pilot Version already in use')
              } else {
                next()
              }
            }
          })
        }
      }
    },
    pilot_description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    modified_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  })
}
