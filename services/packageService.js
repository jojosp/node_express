const db = require('../models')

const packageService = {
  async getPackages (user) {
    try {
      let packages
      let authuser = await db.users.findByPk(user.user_id)
      if (authuser.role_id === 1) {
        packages = await db.packages.findAndCountAll({
          attributes: ['id', 'package_name', 'isbn', 'iso', 'digi_package_id', 'exportflag', 'created'],
          include: [
            {
              model: db.packagecategories,
              as: 'PackageCategory',
              attributes: ['category_id', 'category_name']
            },
            {
              model: db.users,
              as: 'PackageModifiedBy',
              attributes: ['user_id', 'firstname', 'lastname']
            },
            {
              model: db.users,
              as: 'PackageCreator',
              attributes: ['user_id', 'firstname', 'lastname']
            },
            {
              model: db.pilotversions,
              attributes: ['pilot_id', 'pilot_name']
            }
          ]
        })
        console.log('admin user packages\n')
      }
      if (authuser.role_id === 2) {
        packages = await authuser.getUserPackages()
        console.log('editor user packages\n', packages)
      }
      return packages
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}
module.exports = packageService
