// import expressLoader from './express'
// import sequelizeSync from './sequelize'
// import loggerLoader from './logger'
// import authLoader from './authentication'
// import routerLoader from './router'
const expressLoader = require('./express')
const authInit = require('./authentication')
const {initLogger} = require('./logger')
const sequelizeSync = require('./sequelize')
const routerLoader = require('./router')
const {initSocket} = require('./socket')

module.exports = async (server) => {
  await expressLoader(server)
  console.log('*** Express initialized ***')

  let socketport = await initSocket()
  console.log(`*** Socket server listening @ ${socketport} ***`)

  await authInit(server)
  console.log('*** Session initialized ***')

  await sequelizeSync()
  console.log('*** Sequelize models synced *** ')

  await initLogger(server)
  console.log('*** Morgan logger initialized *** ')

  await routerLoader(server)
  console.log('*** Routes established ***')
}
