// const serviceContainer = require('../services')
const db = require('../models')
var Mylogger = require('../server/main')
var pe = require('../helpers/errorConfig')

const moduleController = {
  createModule (req, res) {
    var newmodule = db.modules.build(req.body.data)
    newmodule.module_order = 1
    db.modules.findOne({
      attributes: [
        [db.modules.sequelize.fn('MAX', db.modules.sequelize.col('module_order')), 'maxorder']
      ],
      where: {
        package_id: newmodule.package_id
      }
    }).then((order) => {
      newmodule.module_order = ++order.dataValues.maxorder
      newmodule.created_by = req.user.user_id
      newmodule.modified_by = req.user.user_id
      newmodule.validate().then(() => {
        newmodule.save().then(() => {
          return res.status(200).json({
            errorflag: false,
            message: 'Module Saved Succesfully'
          })
        }).catch((err) => {
          console.log(pe.render(new Error('One Module Save')))
          console.log(pe.render(err))
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        })
      }).catch((err) => {
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(201).json({
          errorflag: true,
          message: err
        })
      })
    }).catch((err) => {
      console.log(pe.render(err))
    })
  },
  updateModule (req, res) {
    db.modules.findOne({
      where: {
        module_id: req.body.module_id
      }
    }).then((updatemodule) => {
      if (!updatemodule) {
        return res.status(201).json({
          errorflag: true,
          message: 'Module not found'
        })
      } else {
        updatemodule.update({
          module_name: req.body.module_name
        }).then((updatemodule) => {
          return res.status(200).json({
            errorflag: false,
            message: 'Module Updated Succesfully'
          })
        }).catch((err) => {
          if (Object.keys(err).length === 0) {
            err = err.message
          }
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          console.log(pe.render(err))
          return res.status(201).json({
            errorflag: true,
            message: err
          })
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Module')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getPackageModules (req, res) {
    db.modules.findAll({
      include: [{
        as: 'module_createdby',
        model: db.users,
        attributes: ['username']
      },
      {
        as: 'module_modifiedby',
        model: db.users,
        attributes: ['username']
      }
      ],
      where: {
        package_id: req.body.id
      },
      attributes: ['package_id', 'module_id', 'module_name', 'module_order', 'updatedAt', 'createdAt'],
      order: ['module_order'],
      limit: req.body.paginationlimit,
      offset: req.body.paginationoffset
    }).then((modules) => {
      return res.status(200).json({
        errorflag: false,
        modules: modules
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find All Modules')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getPackageModules2 (req, res) {
    db.modules.findAll({
      where: {
        package_id: req.params.id
      }
    }).then((findmodule) => {
      return res.status(200).json({
        errorflag: false,
        module: findmodule
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Module')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  insertModule (req, res) {
    var newmodulearray = []
    var validpromises = []
    var savepromises = []
    req.body.forEach(function (item) {
      if (item.module_id) {
        db.modules.findOne({
          where: {
            module_id: item.module_id
          }
        }).then((updatemodule) => {
          updatemodule.update({
            module_order: item.module_order,
            modified_by: req.user.user_id
          })
        })
      } else {
        var newmodule = db.modules.build({
          package_id: item.package_id,
          module_name: item.module_name,
          module_order: item.module_order,
          created_by: req.user.user_id,
          modified_by: req.user.user_id,
          module_status: 1
        })
        newmodulearray.push({
          newmodule: newmodule
        })
      }
    })
    for (var i = 0; i < newmodulearray.length; i++) {
      var validpromise = newmodulearray[i].newmodule.validate()
      validpromises.push(validpromise)
    }
    Promise.all(validpromises).then(() => {
      for (var j = 0; j < newmodulearray.length; j++) {
        var savepromise = newmodulearray[j].newmodule.save()
        savepromises.push(savepromise)
      }
      Promise.all(savepromises).then(() => {
        return res.status(200).json({
          errorflag: false,
          message: 'All Modules Saved Succesfully'
        })
      }).catch((err) => {
        console.log(pe.render(new Error('Save Multiples Modules')))
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        if (Object.keys(err).length === 0) {
          err = err.message
          return res.status(201).json({
            errorflag: true,
            message: err
          })
        } else {
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        }
      })
    }).catch((err) => {
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(201).json({
        errorflag: true,
        message: err
      })
    })
  },
  allModules (req, res) {
    db.modules.findAll({
      attributes: ['module_id', 'module_name']
    }).then((modules) => {
      return res.status(200).json({
        errorflag: false,
        modules: modules
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find All Modules')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  countModules (req, res) {
    db.modules.count({
      where: {
        package_id: req.body.id
      }
    }).then((count) => {
      return res.status(200).json({
        errorflag: false,
        count: count
      })
    }).catch(err => {
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  deleteModule (req, res) {
    db.modules.findOne({
      where: {
        module_id: req.body.id
      }
    }).then((deletemodule) => {
      if (!deletemodule) {
        console.log(pe.render('Module not found'))
        return res.status(201).json({
          errorflag: true,
          message: 'Module not found'
        })
      } else {
        deletemodule.destroy().then(() => {
          db.modules.findAll({
            where: {
              $and: [{
                module_order: {
                  $gt: deletemodule.module_order
                }
              }, {
                package_id: deletemodule.package_id
              }]
            }
          }).then((modules) => {
            modules.forEach((item) => {
              item.decrement(['module_order'], {
                by: 1
              })
            })
            return res.status(200).json({
              errorflag: false,
              message: 'Modules Deleted Succesfully'
            })
          })
        })
      }
    }).catch(err => {
      console.log(pe.render(new Error('Find One Module')))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  myRecentModules (req, res) {
    db.modules.findAll({
      where: {
        package_id: { $in: req.body.packagesid }
      },
      order: [[db.Sequelize.col('updatedAt'), 'DESC']],
      limit: req.body.paginationlimit,
      offset: req.body.paginationoffset
    }).then((modules) => {
      if (modules.length < 1) {
        return res.status(201).json({
          errorflag: true,
          message: 'Modules not Found'
        })
      } else {
        return res.status(200).json({
          errorflag: false,
          modules: modules
        })
      }
    }).catch((error) => {
      console.log(pe.render(new Error('Find Recent Module')))
      console.log(pe.render(error))
      Mylogger.mylogger.SequelizeLogger.error(error.message)
      return res.status(500).json({
        errorflag: true,
        message: error
      })
    })
  },
  copyModule (req, res) {
    var newmodule = db.modules.build({
      module_name: req.body.module_name,
      package_id: req.body.package_id,
      module_status: 1
    })
    newmodule.module_order = 1
    db.modules.findOne({
      attributes: [
        [db.modules.sequelize.fn('MAX', db.modules.sequelize.col('module_order')), 'maxorder']
      ],
      where: {
        package_id: newmodule.package_id
      }
    }).then((order) => {
      newmodule.module_order = ++order.dataValues.maxorder
      newmodule.created_by = req.user.user_id
      newmodule.modified_by = req.user.user_id
      newmodule.validate().then(() => {
        newmodule.save().then(() => {
          db.pages.findAll({
            where: {
              module_id: req.body.module_id
            }
          }).then((pages) => {
            var newpagearray = []
            var validpromises = []
            var savepromises = []
            pages.forEach((page) => {
              var newpage = db.pages.build({
                module_id: newmodule.module_id,
                page_name: page.page_name,
                page_order: page.page_order,
                page_json: page.page_json,
                digi_activity_id: page.digi_activity_id,
                digi_activity_name: page.digi_activity_name,
                digi_module_id: page.digi_module_id,
                digi_module_name: page.digi_module_name,
                digi_skilltype_id: page.digi_skilltype_id,
                digi_skilltype_name: page.digi_skilltype_name,
                digi_keywords: page.digi_keywords,
                digi_category: page.digi_category,
                digi_weight: page.digi_weight,
                digi_eleclimitation: page.digi_eleclimitation,
                created_by: req.user.user_id,
                modified_by: req.user.user_id,
                locked: 0,
                locked_by: 0
              })
              newpagearray.push({
                newpage: newpage
              })
            })
            for (var i = 0; i < newpagearray.length; i++) {
              var validpromise = newpagearray[i].newpage.validate()
              validpromises.push(validpromise)
            }
            Promise.all(validpromises).then(() => {
              for (var j = 0; j < newpagearray.length; j++) {
                var savepromise = newpagearray[j].newpage.save()
                savepromises.push(savepromise)
              }
              Promise.all(savepromises).then(() => {
                return res.status(200).json({
                  errorflag: false,
                  message: 'All Pages Saved Succesfully'
                })
              })
            }).catch((err) => {
              console.log(pe.render(new Error('Module Copied Pages')))
              console.log(pe.render(err))
              Mylogger.mylogger.SequelizeLogger.error(err.message)
              return res.status(500).json({
                errorflag: true,
                message: err
              })
            })
          }).catch((err) => {
            console.log(pe.render(new Error('Module Copied Find AllPages')))
            console.log(pe.render(err))
            Mylogger.mylogger.SequelizeLogger.error(err.message)
            return res.status(500).json({
              errorflag: true,
              message: err
            })
          })
        })
      }).catch((err) => {
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(201).json({
          errorflag: true,
          message: err
        })
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Module Copied')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  }

}

module.exports = moduleController
