/* eslint-disable class-methods-use-this,prefer-promise-reject-errors */
const db = require('../models')

const authenticationService = {
  async signUp (newUser) {
    let user = [null, false]
    try {
      user[0] = await db.users.findOne({
        where: {
          firstname: newUser.firstname,
          lastname: newUser.lastname,
          email: newUser.email,
          username: newUser.username
        }
      })
      if (!user[0]) {
        user = await db.users.findOrCreate({
          where: {
            firstname: newUser.firstname,
            lastname: newUser.lastname,
            email: newUser.email,
            username: newUser.username,
            password: newUser.password,
            role_id: newUser.role_id
          }
        })
      }
    } catch (err) {
      return Promise.reject(err)
    }

    return Promise.resolve(user)
  },
  async logInJWT (userToLogin) {
    let token = null
    const user = await await db.users.findOne({
      where: {
        username: userToLogin.username
      }
    })
    if (!user) {
      return Promise.reject('User not found')
    }
    token = db.users.generateJWT(user)
    const passMatches = await user.verifyPassword(userToLogin.password)
    if (!passMatches) {
      return Promise.reject('Incorrect password or username')
    }
    return Promise.resolve({
      user,
      token
    })
  },
  async logInAs (username) {
    let token = null
    const user = await await db.users.findOne({
      where: {
        username
      }
    })
    if (!user) {
      return Promise.reject('User not found')
    }
    token = db.users.generateJWT(user)

    return Promise.resolve({
      user,
      token
    })
  }
}
module.exports = authenticationService
// export default authenticationService
