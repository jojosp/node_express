var fs = require('fs')
var pe = require('../helpers/errorConfig')
var ncp = require('ncp').ncp
var rimraf = require('rimraf')

const dirtosave = '../projects_dist'
const mask = 777
// function to copy pilot dist folder for each new package
exports.copypackage = function (packagefolder, copydir, next) {
  let dir = dirtosave + '/' + packagefolder + '/'
  ncp(dirtosave + '/' + copydir, dirtosave + '/' + packagefolder + '/', function (err) {
    if (err) {
      console.log(pe.render(err))
      next(true, dir)
    }
    next(false, dir)
  })
}
// function to create directory for each user to save all project folders if not exists
exports.createpackage = function (packagefolder, next) {
  let dir = dirtosave + '/' + packagefolder + '/'
  fs.mkdir(dir, mask, function (err) {
    if (err) {
      console.log(pe.render(err))
      if (err.code === 'EEXIST') {
        next(false, dir)
      } else {
        next(true, err)
      }
    }
    next(false, dir)
  })
}
// function to delete folder package
exports.deletefolder = function (packagefolder, next) {
  let dir = dirtosave + '/' + packagefolder + '/'
  rimraf(dir, function (err) {
    if (err) {
      console.log(pe.render(err))
      next(true, err)
    } else {
      console.log('delete folder')
      next(false, dir + ' deleted successfully')
    }
  })
}
