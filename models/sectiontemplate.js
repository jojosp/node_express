module.exports = function (sequelize, DataTypes) {
  return sequelize.define('sectiontemplate', {
    sectiontemplate_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    section_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    section_key: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sectiontemplate_json: {
      type: DataTypes.TEXT,
      defaultValue: '[]',
      get: function () {
        return this.getDataValue('sectiontemplate_json')
      },
      set: function (value) {
        this.setDataValue('sectiontemplate_json', JSON.stringify(value))
      }
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    modified_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  })
}
