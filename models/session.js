const uuidv4 = require('uuid/v4')

module.exports = function (sequelize, DataTypes) {
  const Session = sequelize.define('sessions', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    session_id: {
      type: DataTypes.CHAR(64),
      allowNull: false
    },
    expires: {
      type: DataTypes.DATE
    },
    user_id: {
      type: DataTypes.INTEGER
    },
    data: {
      type: DataTypes.STRING
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: true
    }
  })

  Session.associate = (models) => {
    Session.belongsTo(models.users, {
      as: 'sessionUser',
      foreignKey: 'user_id',
      constraints: false
    })
  }

  Session.generateUUID = async () => {
    return uuidv4()
  }

  Session.prototype.refreshExpirationDate = function () {
    let now = new Date()
    now.setDate(now.getDate() + 1) // add 24 hours
    this.expires = now
    this.save().then(() => Promise.resolve(true))
  }

  Session.prototype.getsessionUsername = function () {
    return this.getsessionUser().then(user => {
      return Promise.resolve(user.username)
    }).catch(err => { throw new Error(err) })
  }

  return Session
}
