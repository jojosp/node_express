module.exports = (req, res, next) => {
  if (req.user.role_id === 1) {
    next()
  } else {
    return res.status(403).json({
      error: true,
      message: 'You are not allowed to access this page'
    })
  }
}
