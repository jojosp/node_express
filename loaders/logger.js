// import morgan from 'morgan'
// import path from 'path'
// import rfs from 'rotating-file-stream'

const fs = require('fs')
const path = require('path')
const morgan = require('morgan')
const rfs = require('rotating-file-stream')
const { createLogger, format, transports } = require('winston')
const { combine, timestamp, label, prettyPrint } = format
require('winston-daily-rotate-file')

const morganlogDirectory = path.join(__dirname, '../../', 'logger/')
const logDirectory = path.join(__dirname, '../../', 'logger/')

const morganerrorfs = rfs('morganerror.log', {
  interval: '1d', // rotate daily
  path: morganlogDirectory
})
const morgansuccessfs = rfs('morgansuccess.log', {
  interval: '1d', // rotate daily
  path: morganlogDirectory
})

const rotateSuccessTransport = new (transports.DailyRotateFile)({
  filename: logDirectory + '%DATE%.SEQUELIZE_' + 'loggerfile_winston.log',
  dirname: logDirectory,
  datePattern: 'YYYY-MM-DD',
  maxSize: '1m',
  maxFiles: '180d',
  level: 'info'
})
const rotateErrorTransport = new (transports.DailyRotateFile)({
  filename: logDirectory + '%DATE%.SEQUELIZE_ERROR_' + 'loggerfile_winston.log',
  dirname: logDirectory,
  datePattern: 'YYYY-MM-DD',
  maxSize: '1m',
  maxFiles: '180d',
  level: 'error'
})

rotateSuccessTransport.on('rotate', function (oldFilename, newFilename) {
  console.log(`rotated from '${oldFilename}' to '${newFilename}'`)
})
rotateErrorTransport.on('rotate', function (oldFilename, newFilename) {
  console.log(`rotated from '${oldFilename}' to '${newFilename}'`)
})

class MyLogger {
  constructor () {
    this.mylogger = {
      SequelizeLogger: null
    }
    this.getSequelizeLogger()
  }
  getSequelizeLogger () {
    fs.access(logDirectory, fs.constants.F_OK | fs.constants.W_OK, (error) => {
      if (error) {
        console.error(error.code === 'ENOENT' ? 'does not exist' : 'is read-only')
      } else {
        this.mylogger.SequelizeLogger = createLogger({
          format: combine(
            label({ label: 'SEQUELIZE LOGGER' }),
            timestamp(),
            prettyPrint()
          ),
          transports: [
            rotateSuccessTransport,
            rotateErrorTransport
          ]
        })
      }
    })
  }
}

var logger = new MyLogger()

const initLogger = async (server) => {
  server.use(
    morgan('combined', {
      stream: morganerrorfs,
      skip: function (req, res) {
        return res.statusCode < 400
      }
    })
  )
  server.use(
    morgan('combined', {
      stream: morgansuccessfs,
      skip: function (req, res) {
        return res.statusCode > 400
      }
    })
  )
}

module.exports = {logger, initLogger}
