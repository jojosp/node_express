var _ = require('lodash')
module.exports = function (sequelize, DataTypes) {
  const Package = sequelize.define('packages', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    package_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isUniquePackagename (value, next) {
          sequelize.query('SELECT count(1) as counter FROM packages WHERE package_name = ? ', {
            replacements: [value],
            type: sequelize.QueryTypes.SELECT
          }).then(packages => {
            if (packages[0].counter > 0) {
              next('Package name is already in use')
            } else {
              next()
            }
          })
        },
        is: {
          args: /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/i,
          msg: 'This is not valid Package Name'
        }
      }
    },
    setting_json: {
      type: DataTypes.TEXT,
      defaultValue: '[]',
      get: function () {
        return this.getDataValue('setting_json')
      },
      set: function (value) {
        this.setDataValue('setting_json', JSON.stringify(value))
      }
    },
    package_category: {
      type: DataTypes.STRING,
      allowNull: false
    },
    package_folder: {
      type: DataTypes.STRING,
      allowNull: false
    },
    image: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isbn: {
      type: DataTypes.STRING,
      allowNull: false
    },
    format: {
      type: DataTypes.STRING,
      allowNull: false
    },
    version: {
      type: DataTypes.DECIMAL(2, 1),
      allowNull: false
    },
    iso: {
      type: DataTypes.STRING,
      allowNull: false
    },
    download_videos: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    assigned: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    package_status: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    digi_package_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    modified_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    exportflag: {
      type: DataTypes.INTEGER,
      allowNull: true,
      default: 0
    },
    lastexport: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    hooks: {
      beforeDestroy: (packages, options) => {
        return sequelize.query('SELECT exportflag FROM packages WHERE id = ?', {
          replacements: [packages.id],
          type: sequelize.QueryTypes.SELECT
        }).then((data) => {
          if (data[0].exportflag === 1) {
            throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
          }
        }).catch((error) => {
          throw new Error(error)
        })
      },
      beforeUpdate: (packages, options) => {
        return sequelize.Promise.try(() => {
          return sequelize.query('SELECT exportflag FROM packages WHERE id = ?', {
            replacements: [packages.id],
            type: sequelize.QueryTypes.SELECT
          }).then((data) => {
            let flag = _.get(packages, '_changed.exportflag')
            if (!flag) {
              if (data[0].exportflag === 1) {
                throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
              } else {
                if (packages._changed.version) {
                  if (packages.version < packages._previousDataValues.version) {
                    throw new Error('Package Version must be greater than the previous one!')
                  }
                }
              }
            }
          }).catch((error) => {
            throw new Error(error)
          })
        })
      }
    },
    indexes: [{
      unique: true,
      fields: ['package_name', 'package_folder']
    }]
  })

  Package.associate = (models) => {
    Package.hasMany(models.modules, {
      foreignKey: 'package_id',
      sourceKey: 'id'
    })
    // Package.belongsTo(models.users, {
    //   as: 'packagemodified',
    //   foreignKey: 'modified_by',
    //   sourceKey: 'user_id'
    // })
    Package.belongsToMany(models.users, {
      through: {
        model: models.packagestousers,
        unique: true
      },
      as: 'packageuser',
      foreignKey: 'package_id',
      // foreignKey: 'package_id',
      // sourceKey: 'id',
      constraints: false
    })
    Package.belongsToMany(models.sections, {
      through: {
        model: models.sectionstopackages,
        unique: false
      },
      as: 'packagesection',
      foreignKey: 'package_id',
      sourceKey: 'id',
      constraints: false
    })
    Package.belongsTo(models.pilotversions, {
      foreignKey: 'lastexport',
      sourceKey: 'pilot_id'
    })
    // jo
    Package.hasMany(models.bug, {
      as: 'Bugs',
      foreignKey: 'package_id',
      constraints: false})

    Package.belongsTo(models.users, {
      as: 'PackageModifiedBy',
      foreignKey: 'modified_by',
      sourceKey: 'user_id'
    })

    Package.belongsTo(models.users, {
      as: 'PackageCreator',
      foreignKey: 'created_by',
      sourceKey: 'user_id'
    })

    // Package.hasOne(models.packagecategories, {
    //   as: 'Bugs',
    //   foreignKey: 'package_id',
    //   constraints: false})
    Package.belongsTo(models.packagecategories, {
      as: 'PackageCategory',
      foreignKey: 'package_category',
      sourceKey: 'category_id',
      constraints: false
    })
    // Package.belongsToMany(models.packages, {
    //   as: 'UserPackages',
    //   through: {
    //     model: models.packagestousers,
    //     unique: false
    //   },
    //   foreignKey: 'user_id',
    //   constraints: false
    // })
  }

  return Package
}
