const express = require('express')
const config = require('../config')
const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development'

const appWebSocket = express()
const serverWebsocket = appWebSocket.listen(config[env].socketport)
let io = require('socket.io').listen(serverWebsocket)

const initSocket = async () => Promise.resolve(config[env].socketport)
module.exports = {io, initSocket}
