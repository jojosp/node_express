const nodemailer = require('nodemailer')

const mailService = {

  async generateTestTransport () {
    let testAccount = await nodemailer.createTestAccount()

    let transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass // generated ethereal password
      }
    })
    return Promise.resolve(transporter)
  },
  async sendMail (transporter) {
    let info = await transporter.sendMail({
      from: '"Fred Foo 👻" <foo@example.com>',
      to: 'bar@example.com, baz@example.com',
      subject: 'Hello ✔',
      text: 'Hello world?',
      html: '<b>Hello world?</b>'
    })
    return Promise.resolve(info)
  },
  async getPreviewUrl (info) {
    const url = nodemailer.getTestMessageUrl(info)
    console.log('Preview URL: %s', url)
    return Promise.resolve(url)
  }

}
module.exports = mailService
