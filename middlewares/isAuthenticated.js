// ONLY FOR SESSION
// function isAuthennticated (req, res, next) {
//   if (req.isAuthenticated()) {
//     next()
//   } else {
//     return res.status(200).json({ error: true, message: 'User not authenticated' })
//   }
// }
const passport = require('passport')
const isAuthenticated = passport.authenticate('jwt', {session: false})
module.exports = isAuthenticated
