var fs = require('fs')
var path = require('path')
const { createLogger, format, transports } = require('winston')
const { timestamp, printf } = format
var exportLogDirectory = path.join(__dirname, '../../', 'logger/exports/')
// const env = process.env.NODE_ENV || 'dev'
require('winston-daily-rotate-file')

if (!fs.existsSync(exportLogDirectory)) {
  fs.mkdirSync(exportLogDirectory)
}

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `AT ${timestamp} WITH LEVEL [${level}]:
  {${message}}`
})

const dailyRotationExportLogTransport = new transports.DailyRotateFile({
  filename: `${exportLogDirectory}/%DATE%_exports.log`,
  datePattern: 'YYYY-MM-DD',
  dirname: exportLogDirectory
})
const exportsLogger = createLogger({
// level: env === 'development' ? 'debug' : 'info',
  level: 'info',
  format: format.combine(
    timestamp(),
    myFormat
  ),
  transports: [dailyRotationExportLogTransport]
})
module.exports = exportsLogger
