const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports = function (sequelize, DataTypes) {
  const User = sequelize.define('users', {
    user_id: {
      // type: DataTypes.UUID,
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    firstname: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        isAlpha: true
      }
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        isAlpha: true
      }
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        is: {
          args: /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/i,
          msg: 'This is not valid username'
        },
        isUniqueUsername (value, next) {
          sequelize.query('SELECT count(1) as counter FROM users WHERE username = ? ', {
            replacements: [value],
            type: sequelize.QueryTypes.SELECT
          }).then(users => {
            if (users[0].counter > 0) {
              next('Usename is already in use')
            } else {
              next()
            }
          })
        }
      }
    },
    password: {
      type: DataTypes.CHAR(64),
      // type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
        // len: [4, 40]
        // isPasswordNotNull (value, next) {
        //   if (value !== undefined) {
        //     if (value.length > 6) {
        //       next()
        //     } else {
        //       next('This is short password')
        //     }
        //   } else {
        //     next('This is not valid password')
        //   }
        // }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          args: true,
          msg: 'This is not valid email'
        }
        // isUniqueEmail (value, next) {
        //   sequelize.query('SELECT count(1) as counter FROM users WHERE email = ? ', {
        //     replacements: [value],
        //     type: sequelize.QueryTypes.SELECT
        //   }).then(users => {
        //     if (users[0].counter > 0) {
        //       next('Email is already in use')
        //     } else {
        //       next()
        //     }
        //   })
        // }
      }
    },
    role_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    modified_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    user_status: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    indexes: [{
      unique: true,
      fields: ['username', 'email']
    }],
    hooks: {
      beforeCreate: async (user, options) => {
        console.log('before', user.password)
        user.password = await User.generateHashPassword(user.password)
        console.log('after', user.password)
      },
      beforeUpdate: async (user, options) => {
        if (user.changed('password')) {
          user.password = await User.generateHashPassword(user.password)
        }
      }
    }
  },
  sequelize.addHook('beforeFind', (options) => {
    if (!options.attributes) {
      options.attributes = {}
      options.attributes.exclude = ['createdAt']
    }
  })
  )

  User.associate = (models) => {
    User.belongsTo(models.roles, {
      foreignKey: 'role_id'
    })
    User.belongsTo(models.sessions, {
      foreignKey: 'user_id'
    })
    User.belongsToMany(models.packages, {
      through: {
        model: models.packagestousers,
        unique: true
      },
      // as: 'userpackage',
      as: 'packageuser',
      foreignKey: 'user_id',
      constraints: false
    })
    User.hasMany(models.bug, {
      as: 'Bugs',
      foreignKey: 'bug_issuer',
      constraints: false
    })
    User.hasMany(models.packages, {
      as: 'UserCreatedPackages',
      foreignKey: 'created_by',
      constraints: false
    })
  }

  User.findByUsername = async (name) => {
    let user
    try {
      user = await User.findOne({
        where: { username: name }
      })
    } catch (err) {
      console.log(err)
    }
    return Promise.resolve(user)
  }

  User.generateHashPassword = (password) => {
    return bcrypt.hash(password, 10)
  }

  User.prototype.verifyPassword = function (pass) {
    const hashpassword = require('crypto').createHash('sha256').update(pass).digest('base64')
    if (this.password === hashpassword) {
      return Promise.resolve(true)
    }
    return bcrypt.compare(pass, this.password).then((passwordMatches) => {
      return Promise.resolve(passwordMatches)
    })
  }
  // for session
  // User.prototype.toMinimumFields = function () {
  //   return {
  //     user_id: this.user_id,
  //     firstname: this.firstname,
  //     role_id: this.role_id
  //   }
  // }

  // for JWT
  User.prototype.toMinimumFields = function () {
    return {
      user_id: this.user_id,
      firstname: this.firstname,
      role_id: this.role_id
    }
  }

  User.prototype.toMediumFields = function () {
    return {
      username: this.username,
      firstname: this.firstname,
      lastname: this.lastname,
      email: this.email
    }
  }

  User.toAuthJson = (user) => {
    return {
      user_id: user.user_id,
      firstname: user.firstname,
      role_id: user.role_id
    }
  }

  User.prototype.generateJWT = function () {
    const data = {
      id: this.user_id,
      firstname: this.firstname,
      role: this.role_id
    }
    const signature = process.env.JWT_SECRET
    const expiration = '5m'

    return jwt.sign({ data }, signature, { expiresIn: expiration })
  }

  User.generateJWT = (user) => {
    const data = {
      id: user.user_id,
      firstname: user.firstname,
      role: user.role_id
    }
    const signature = process.env.JWT_SECRET
    const expiration = '5m'

    return jwt.sign({ data }, signature, { expiresIn: expiration })
  }

  return User
}
