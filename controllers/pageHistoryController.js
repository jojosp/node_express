var pe = require('../helpers/errorConfig')
const db = require('../models')
var Mylogger = require('../server/main')

const pageHistoryController = {
  createPageHistory (req, res) {
    var newpage = db.pagehistory.build(req.body.data)
    newpage.created_by = req.user.user_id
    newpage.modified_by = req.user.user_id
    newpage.validate().then(() => {
      newpage.save().then(() => {
        return res.status(200).json({
          errorflag: false,
          message: 'Page Saved Succesfully',
          page: newpage
        })
      }).catch((err) => {
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        console.log(pe.render(new Error('Save Page')))
        if (Object.keys(err).length === 0) {
          err = err.message
          return res.status(201).json({
            errorflag: true,
            message: err
          })
        } else {
          console.log(pe.render(err))
          return res.status(500).json({
            erroflag: true,
            message: err
          })
        }
      })
    }).catch((err) => {
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
      return res.status(201).json({
        erroflag: true,
        message: err
      })
    })
  },
  getPageHistory (req, res) {
    db.pageshistory.findAll({
      distinct: true,
      include: {
        model: db.users,
        attributes: ['username']
      },
      where: {
        page_id: req.params.id
      },
      order: [[db.Sequelize.col('updatedAt'), 'DESC']]
    }).then((pagehistory) => {
      if (!pagehistory) {
        return res.status(201).json({
          errorflag: true,
          message: 'History Page is empty'
        })
      } else {
        return res.status(200).json({
          errorflag: false,
          message: 'Page found',
          pagehistory: pagehistory
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Page')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  deletePageHistory (req, res) {
    db.pageshistory.destroy({
      where: {
        pagehistory_id: req.params.id
      }
    }).then(() => {
      return res.status(200).json({
        errorflag: false,
        message: 'History Page Deleted'
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Page')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  }
}

module.exports = pageHistoryController
