require('dotenv').config()

module.exports = {
  development: {
    name: 'My App Name',
    port: 3000,
    dbname: 'digi',
    dbuser: 'jojo',
    dbpwd: 'jojo',
    host: 'localhost',
    dialect: 'mysql',
    domain: 'localhost:8080',
    // domain: 'localhost:3000',
    dbport: 3306,
    socketport: 3001,
    jwtSecret: process.env.JWT_SECRET,
    storeurl: process.env.SESSION_DB_URI,
    sessionSecret: process.env.SESSION_SECRET
  },
  production: {
    name: 'My App Name',
    port: process.env.PORT,
    dbname: process.env.DBNAME,
    dbuser: process.env.DBUSER,
    dbpwd: process.env.DBPWD,
    host: process.env.HOST,
    dialect: process.env.DIALECT,
    domain: process.env.DOMAIN,
    dbport: process.env.DBPORT,
    socketport: process.env.SOCKETPORT,
    jwtSecret: process.env.JWT_SECRET,
    storeurl: process.env.SESSION_DB_URI,
    sessionSecret: process.env.SESSION_SECRET
  }
}
