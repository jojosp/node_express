
// import controllerContainer from '../controllers/controllerContainer'
var controllerContainer = require('../controllers/controllerContainer')
const userRouter = require('express').Router()
const middlewares = require('../middlewares')

userRouter.use(middlewares.isAuthenticated)
// jo
userRouter.get('/info', controllerContainer.userController.getUser)
// end point to retrieve all availables roles
userRouter.post('/roles', controllerContainer.userController.getRoles)
// endpoint Profile logged in user
userRouter.post('/profile', controllerContainer.userController.getCurrentUserProfile)
// endpoint update user with requested id
userRouter.post('/update', controllerContainer.userController.updateUser)
// endpoint find user by username
userRouter.get('/ui/:username', controllerContainer.userController.getUserByUsername)
// endpoint Profile based on given id
userRouter.get('/profile/:id', middlewares.isAdmin, controllerContainer.userController.getUserProfile)
// endpoint to create users
userRouter.post('/create', middlewares.isAdmin, controllerContainer.userController.createUser)
// search in all fields
userRouter.post('/usersandpackages', middlewares.isAdmin, controllerContainer.userController.userAndPackages)
// end point to return all users
userRouter.post('/gridview', middlewares.isAdmin, controllerContainer.userController.gridView)
// end point to ban user
userRouter.post('/banuser', middlewares.isAdmin, controllerContainer.userController.banUser)

module.exports = userRouter
