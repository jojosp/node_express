module.exports = function (sequelize, DataTypes) {
  const Role = sequelize.define('roles', {
    role_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    role_name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: false
  })

  Role.associate = (models) => {
    Role.belongsTo(models.users, {
      as: 'Role',
      foreignKey: 'role_id',
      constraints: false})
  }

  return Role
}
